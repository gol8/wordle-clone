# Wordle clone, written in C++

A very barebones clone for the terminal of the popular web game Wordle, written in C++

## Usage

The list of words used by the official Wordle game come with this repo and are used by default.
If you wish to use your own word list you need to supply a newline (`\n`)
delimited list of (ASCII) words as the first argument to this program.

`./wordle ./you_word_list.txt`


## Building

`g++ ./wordle.cpp -o ./wordle`
