#include <iostream>
#include <cctype>
#include <ctime>
#include <vector>
#include <map>
#include <fstream>

#define WORD_SIZE 5

enum Hint {
    MATCH = '*',
    CONTAINS = '?',
    NOMATCH = '_'
};

class InputFileError {};

using std::string;

string makePatternString(const string&, const string&);
bool isInputValid(const string&, const std::vector<string>&);
std::vector<string>& parseInputFile(const char*, std::vector<string>&);

bool isInputValid(const string& guess, const std::vector<string>& valid_words) {
    if(guess.size() != WORD_SIZE) return false;

    for(size_t idx = 0; idx < guess.size(); idx++) {
        if(!isalpha(guess[idx])) return false;
    }

    bool found = false;
    for(size_t idx = 0; idx < valid_words.size(); idx++) {
        if(guess == valid_words[idx]) found = true;
    }

    if(!found) {
        std::cerr << guess << " not in list of valid words\n";
        return false;
    }


    return true;
}

string makePatternString(const string& guess, const string& target) {
    string pattern;
    std::map<char, int> seen_chars;

    for(size_t idx = 0; idx < target.size(); idx++) {
        if(guess[idx] == target[idx]) {
            pattern.push_back(MATCH);
        }

        else if(!seen_chars[guess[idx]] && target.find(guess[idx]) != string::npos) {
            seen_chars[guess[idx]] = 1;
            pattern.push_back(CONTAINS);
        }

        else pattern.push_back(NOMATCH);
    }

    return pattern;
}

std::vector<string>& parseInputFile(const char* fpath, std::vector<string>& invec) {
    std::ifstream input_file(fpath);

    if(!input_file) throw InputFileError();

    string word;
    while(input_file >> word) invec.push_back(word);
    return invec;
}

int main(int argc, char **argv) {
    static const size_t max_attempt = 6;
    static const char* def_input_words = "./words.list";
    static const char* input_words = argc > 1 ? argv[1] : def_input_words;

    std::vector<string> words;

    try {
        words = parseInputFile(input_words, words);
    }
    catch(InputFileError) {
        std::cerr << "Failed to open/parse " << input_words << '\n';
        return 1;
    }

    srand(time(NULL));

    size_t index = rand() % words.size();
    string target = words[index];
    size_t attempt = 1;
    string guess;

    bool found = false;

    while(attempt <= max_attempt && std::cin) {
        std::cout << "[" << attempt  << "]: ";
        std::cin >> guess;

        if(!isInputValid(guess, words)) {
            std::cerr << "Invalid input! Try again.\n";
            continue;
        }

        if(guess == target) {
            std::cout << "Success! You guessed the word " << target << " in " << attempt << " attempt(s)\n";
            found = true;
            break;
        }

        std::cout << makePatternString(guess, target) << '\n';
        attempt++;
    }

    if(!found) std::cout << "Word was: " << target << '\n';

    return 0;
}
